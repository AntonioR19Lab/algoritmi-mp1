package it.unicam.cs.asdl1920.mp1;

import java.util.Objects;

/**
 * Una facility generica è una caratteristica o delle dotazioni che una certa
 * aula può avere. La classe va specificata ulteriormente per definire i diversi
 * tipi di facilities.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public abstract class Facility {

    private final String codice;

    private final String descrizione;

    /**
     * Costruisce una certa facility generica.
     * 
     * @param codice
     *                        identifica la facility univocamente
     * @param descrizione
     *                        descrizione della facility
     * @throws NullPointerException
     *                                  se una qualsiasi delle informazioni
     *                                  richieste è nulla.
     */
    public Facility(String codice, String descrizione) {
        if(codice == null || descrizione == null) throw new NullPointerException(); // controllo che non siano stati passati valori nulli
        this.codice = codice; // inizializzo l'attributo codice
        this.descrizione = descrizione; // inizializzo l'attributo descrizione
    }

    /**
     * @return the codice
     */
    public String getCodice() {
        return this.codice;
    }

    /**
     * @return the descrizione
     */
    public String getDescrizione() {
        return this.descrizione;
    }

    /*
     * Ridefinire in accordo con equals
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.getCodice());
    } // utilizzo il metodo hash di Object per definire hashcode in accordo con equals

    /*
     * L'uguaglianza di due facilities è basata unicamente sul codice
     */
    @Override
    public boolean equals(Object object) {
        if (this == object) return true; // se this è utuale a object allora si tratta dello stesso oggetto
        if (!(object instanceof Facility)) return false; // se object non è di tipo Facility la condizione non più essere soddisfatta
        Facility facility = (Facility) object; // casto object in un oggetto di tipo Facility
        return Objects.equals(getCodice(), facility.getCodice());
    }

    @Override
    public String toString() {
        return "Facility [codice=" + codice + ", descrizione=" + descrizione
                + "]";
    }

    /**
     * Determina se questa facility soddisfa un'altra facility data. Ad esempio
     * se questa facility indica che è presente un proiettore HDMI, allora essa
     * soddisfa la facility "presenza di un proiettore HDMI". Un altro esempio:
     * se questa facility indica un numero di posti a sedere pari a 30, allora
     * essa soddisfa ogni altra facility che indica che ci sono un numero di
     * posti minore o uguale a 30. Il metodo dipende dal tipo di facility, per
     * questo è astratto e va definito nelle varie sottoclassi.
     * 
     * @param o
     *              l'altra facility con cui determinare la compatibilità
     * @return true se questa facility soddisfa la facility passata, false
     *         altrimenti
     * @throws NullPointerException
     *                                  se la facility passata è nulla.
     */
    public abstract boolean satisfies(Facility o);

}
