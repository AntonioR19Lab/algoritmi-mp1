/**
 * 
 */
package it.unicam.cs.asdl1920.mp1;

/**
 * Una Quantitative Facility è una facility che contiene una informazione
 * quantitativa. Ad esempio la presenza di 50 posti a sedere oppure la presenza
 * di 20 thin clients.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public class QuantitativeFacility extends Facility {

    private final int quantity;

    /**
     * Costruisce una facility quantitativa.
     * 
     * @param codice
     *                        codice identificativo della facility
     * @param descrizione
     *                        descrizione testuale della facility
     * @param quantity
     *                        quantita' associata alla facility
     * @throws NullPointerException
     *                                  se una qualsiasi delle informazioni
     *                                  {@code codice} e {@code descrizione} è
     *                                  nulla.
     */
    public QuantitativeFacility(String codice, String descrizione,
            int quantity) {
        super(codice, descrizione); // utilizzo il costruttore della super classe
        this.quantity = quantity; // inizializzo attributo quantity
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return this.quantity;
    }

    /*
     * Questa quantitative facility soddisfa una facility data se e solo se la
     * facility data è una quantitative facility, ha lo stesso codice e la
     * quantità associata a questa facility è maggiore o uguale alla quantità
     * della facility data.
     */
    @Override
    public boolean satisfies(Facility o) {
    	if(o == null) throw new NullPointerException(); // verifico che non siano stati passati vaolri nulli
        if(o instanceof QuantitativeFacility){ // controllo che o sia di tipo QuantitativeFacility
        	QuantitativeFacility obj = (QuantitativeFacility) o; // eseguo il cast di o attribuendolo ad un oggetto di tipo QuantitativeFacility
            if(obj.getCodice().equals(this.getCodice()) && obj.getQuantity() <= this.getQuantity() ) return true; // torno true se il codice e la quantità sono uguali
        }
        return false; // altrimenti torno false
    }

}
