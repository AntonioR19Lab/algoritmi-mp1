package it.unicam.cs.asdl1920.mp1;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

/**
 * Un time slot Ã¨ un intervallo di tempo continuo che puÃ² essere associato ad
 * una prenotazione o a una facility. Gli oggetti della classe sono immutabili.
 * Non sono ammessi time slot che iniziano e finiscono nello stesso istante.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public class TimeSlot implements Comparable<TimeSlot> {

    /**
     * Rappresenta la soglia di tolleranza da considerare nella sovrapposizione
     * di due Time Slot. Se si sovrappongono per un numero di minuti minore o
     * uguale a questa soglia allora NON vengono considerati sovrapposti.
     */
    public static final int MINUTES_OF_TOLERANCE_FOR_OVERLAPPING = 5;

    private final GregorianCalendar start;

    private final GregorianCalendar stop;

    /**
     * Crea un time slot tra due istanti di inizio e fine
     *
     * @param start
     *                  inizio del time slot
     * @param stop
     *                  fine del time slot
     * @throws NullPointerException
     *                                      se uno dei due istanti, start o
     *                                      stop, Ã¨ null
     * @throws IllegalArgumentException
     *                                      se start Ã¨ uguale o successivo a
     *                                      stop
     */
    public TimeSlot(GregorianCalendar start, GregorianCalendar stop) {
        if(start == null || stop == null) throw new NullPointerException(); // verifico che non siano stati passati valori nulli
        if( start.getTimeInMillis() >= stop.getTimeInMillis()) throw new IllegalArgumentException(); // verifico che i millisecondi di start siano minori dei millisecodni di stop
        this.start = start; // inizializzo attributo start
        this.stop = stop; // inizializzo attributo stop
    }

    /**
     * @return the start
     */
    public GregorianCalendar getStart() {
        return this.start;
    }

    /**
     * @return the stop
     */
    public GregorianCalendar getStop() {
        return this.stop;
    }

    /*
     * Ridefinire in accordo con equals
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.getStart(), this.getStop()); // utilizzo il metodo hash per ridefinire hashcode in accordo con equals
    }

    /*
     * Un time slot Ã¨ uguale a un altro se rappresenta esattamente lo stesso
     * intervallo di tempo, cioÃ¨ se inizia nello stesso istante e termina nello
     * stesso istante.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TimeSlot)) return false; // se obj non è di tipo timeslot allora la condizione non può essere verificata
        TimeSlot timeSlot = (TimeSlot) obj; // casto obj in oggetto di tipo TimeSlot
        if(this.getStart().equals(timeSlot.getStart()) // se lo start e lo stop della classe sono uguali allo start e stop dell'oggetto allora torno true
                && this.getStop().equals(timeSlot.getStop())) {
            return true;
        }
        return false; // altrimenti torno false
    }

    /*
     * Un time slot precede un altro se inizia prima. Se due time slot iniziano
     * nello stesso momento quello che finisce prima precede l'altro. Se hanno
     * stesso inizio e stessa fine sono uguali, in compatibilitÃ  con equals.
     */
    @Override
    public int compareTo(TimeSlot o) {
        if (this.start.compareTo(o.start) == 0) { // utilizzo la compareto di calendar per verificare che lo start della classe sia uguale allo start dell'oggetto o
            if (this.stop.compareTo(o.stop) == 0) { // utilizzo la compareto di calendar per verificare che lo stop della classe sia uguale allo stop dell'oggetto o
                return 0; // se così allora torno 0 che sta a significare che sono uguali
            } else if (this.stop.compareTo(o.stop) > 0) { // se this.stop è maggiore di o.stop allora questo viene prima
                return 1;
            } else { // altrimenti è successivo 
                return -1;
            }
        } else if (this.start.compareTo(o.start) > 0 ) { // se this.start è maggiore di o.start allora questo viene prima
            return 1;
        } else { // altrimenti è successivo
            return -1;
        }
    }

    /**
     * Determina se questo time slot si sovrappone a un altro time slot dato,
     * considerando la soglia di tolleranza.
     *
     * @param o
     *              il time slot che viene passato per il controllo di
     *              sovrapposizione
     * @return true se questo time slot si sovrappone per piÃ¹ di
     *         MINUTES_OF_TOLERANCE_FOR_OVERLAPPING minuti a quello passato
     * @throws NullPointerException
     *                                  se il time slot passato Ã¨ nullo
     */
    public boolean overlapsWith(TimeSlot o) {
        // cerco il massimo fra gli start e il minimo fra gli stop
        // verifico la loro sovrapposizione e, se true, calcolo di
        // quanto tempo convertendolo dal timestamp.
        // in fine confronto la soglia di tolleranza
        GregorianCalendar start = (this.start.after(o.start)) ? this.start : o.start;
        GregorianCalendar stop = (this.stop.before(o.stop)) ? this.stop : o.stop;
        // la sovrapposizione esiste quando start <= stop
        if (start.before(stop)) {
            // converto la differenza in minuti
            int minutes = Math.round((float) (stop.getTimeInMillis() - start.getTimeInMillis()) / (60 * 1000));
            if (minutes > TimeSlot.MINUTES_OF_TOLERANCE_FOR_OVERLAPPING) {
                return true;
            }
        }
        return false;
    }
    /*
     * Esempio di stringa: [4/11/2019 11.0 - 4/11/2019 13.0] Esempio di stringa:
     * [10/11/2019 11.15 - 10/11/2019 23.45]
     */
    @Override
    public String toString() {
        return "["
                + this.start.get(Calendar.DAY_OF_MONTH)
                + "/"+(this.start.get(Calendar.MONTH)+1)
                + "/"+this.start.get(Calendar.YEAR)
                + " "+this.start.get(Calendar.HOUR_OF_DAY)
                + "."+this.start.get(Calendar.MINUTE)
                + " - "+this.stop.get(Calendar.DAY_OF_MONTH)
                + "/"+(this.stop.get(Calendar.MONTH)+1)
                + "/"+this.start.get(Calendar.YEAR)
                + " "+this.stop.get(Calendar.HOUR_OF_DAY)
                + "."+this.stop.get(Calendar.MINUTE)
                + "]";
    }

}