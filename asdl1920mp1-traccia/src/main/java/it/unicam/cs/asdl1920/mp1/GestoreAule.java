package it.unicam.cs.asdl1920.mp1;

import java.util.HashSet;
import java.util.Set;

/**
 * Un gestore di aule gestisce un insieme di aule e permette di cercare aule
 * libere con certe caratteristiche fra quelle che gestisce.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public class GestoreAule {

    private final Set<Aula> aule;

    /**
     * Crea un gestore vuoto.
     */
    public GestoreAule() {
        this.aule = new HashSet<Aula>(); // inizializzo attributo aule
    }

    /**
     * Aggiunge un'aula al gestore.
     *
     * @param a una nuova aula
     * @return true se l'aula è stata aggiunta, false se era già presente.
     * @throws NullPointerException se l'aula passata è nulla
     */
    public boolean addAula(Aula a) {
        if (a == null)
            throw new NullPointerException(); // verifico che non siano stati passati valori nulli
        return this.aule.add(a); // aggiungo aula
    }

    /**
     * @return the aule
     */
    public Set<Aula> getAule() {
        return this.aule;
    }

    /**
     * Cerca tutte le aule che soddisfano un certo insieme di facilities e che siano
     * libere in un time slot specificato.
     *
     * @param requestedFacilities insieme di facilities richieste che un'aula deve
     *                            soddisfare
     * @param ts                  il time slot in cui un'aula deve essere libera
     *
     * @return l'insieme di tutte le aule gestite da questo gestore che soddisfano
     *         tutte le facilities richieste e sono libere nel time slot indicato.
     *         Se non ci sono aule che soddisfano i requisiti viene restituito un
     *         insieme vuoto.
     * @throws NullPointerException se una qualsiasi delle informazioni passate è
     *                              nulla
     */
    public Set<Aula> cercaAuleLibere(Set<Facility> requestedFacilities, TimeSlot ts) {
        if (ts == null || requestedFacilities == null)
            throw new NullPointerException(); // verifico che non siano stati passati valori nulli
        Set<Aula> auleReturn = new HashSet<Aula>(); // inizializzo vartiabile contenente tutte le aule libere come vuoto
        for (Aula aula : this.aule) { // ciclo tutte le aule dell'oggetto
            if (aula.satisfiesFacilities(requestedFacilities) && aula.isFree(ts)) { // se le aule soddisfano le facilities e l'aula è libera nello slot di tempo
                auleReturn.add(aula); // aggiungo l'aula all'array da ritornare
            }
        }
        return auleReturn;
    }

}
