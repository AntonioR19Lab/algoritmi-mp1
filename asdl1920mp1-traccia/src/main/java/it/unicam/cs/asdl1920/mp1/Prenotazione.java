package it.unicam.cs.asdl1920.mp1;

import java.util.Objects;

/**
 * Una prenotazione riguarda una certa aula per un certo time slot.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public class Prenotazione implements Comparable<Prenotazione> {

    private final Aula aula;

    private final TimeSlot timeSlot;

    private final String docente;

    private final String motivo;

    /**
     * Costruisce una prenotazione.
     * 
     * @param aula
     *                     l'aula a cui la prenotazione si riferisce
     * @param timeSlot
     *                     il time slot della prenotazione
     * @param docente
     *                     il nome del docente che ha prenotato l'aula
     * @param motivo
     *                     il motivo della prenotazione
     * @throws NullPointerException
     *                                  se uno qualsiasi degli oggetti passati è
     *                                  null
     */
    public Prenotazione(Aula aula, TimeSlot timeSlot, String docente,
            String motivo) {
        if(aula.equals(null) || timeSlot.equals(null) || docente.equals(null) || motivo.equals(null)) throw new NullPointerException(); // verifico che non siano stati passato valori nulli
        this.aula = aula; // inizializzo attributo aula
        this.timeSlot = timeSlot; // inizializzo attributo timeslot
        this.docente = docente; // inizializzo attributo docente
        this.motivo = motivo; // inizializzo attributo motivo
    }

    /**
     * @return the aula
     */
    public Aula getAula() {
        return this.aula;
    }

    /**
     * @return the timeSlot
     */
    public TimeSlot getTimeSlot() {
        return this.timeSlot;
    }

    /**
     * @return the docente
     */
    public String getDocente() {
        return this.docente;
    }

    /**
     * @return the motivo
     */
    public String getMotivo() {
        return this.motivo;
    }

    /*
     * Ridefinire in accordo con equals
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.getAula(), this.getTimeSlot()); // utilzzo metodo hash per definire hashCode in accordo con equals
    }

    /*
     * L'uguaglianza è data solo da stessa aula e stesso time slot. Non sono
     * ammesse prenotazioni diverse con stessa aula e stesso time slot.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true; // se this è uguale a obj allora si tratta dello stesso oggetto
        if (!(obj instanceof Prenotazione)) return false; // se obj non è di tipo Prenotazione allora l'uguaglianza non può essere soddisfatta
        Prenotazione reservation = (Prenotazione) obj; // casto obj in oggetto di tipo Prenotazione
        return this.getAula().equals(reservation.getAula()) &&
                this.getTimeSlot().equals(reservation.getTimeSlot()); // se aula è uguale all'aula della prenotazione e il timeslot è uguale al timeslot della prenotazione torno true altrimenti false
    }

    /*
     * Una prenotazione precede un altra in base all'ordine dei time slot. Se
     * due prenotazioni hanno lo stesso time slot allora una precede l'altra in
     * base all'ordine tra le aule.
     */
    @Override
    public int compareTo(Prenotazione o) {
    	int order = this.getTimeSlot().compareTo(o.getTimeSlot()); // return -1 | 0 | 1  
    	if (order != 0) 
    		return order; // se order è -1 o 1 ritorno order
    	else 
    		return this.getAula().compareTo(o.getAula()); // altrimenti ritorno la compareTo tra aula e aula dell'oggetto passato
    }

    @Override
    public String toString() {
        return "Prenotazione [aula = " + aula + ", time slot =" + timeSlot
                + ", docente=" + docente + ", motivo=" + motivo + "]";
    }

}
