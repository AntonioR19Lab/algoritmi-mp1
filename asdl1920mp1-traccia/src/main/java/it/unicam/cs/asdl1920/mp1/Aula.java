package it.unicam.cs.asdl1920.mp1;

import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * Un oggetto della classe aula rappresenta una certa aula con le sue facilities
 * e le sue prenotazioni.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public class Aula implements Comparable<Aula> {
    // Identificativo unico di un'aula
    private final String nome;

    // Location dell'aula
    private final String location;

    // Insieme delle facilities di quest'aula
    private final Set<Facility> facilities;

    // Insieme delle prenotazioni per quest'aula, segue l'ordinamento naturale
    // delle prenotazioni
    private final SortedSet<Prenotazione> prenotazioni;

    /**
     * Costruisce una certa aula con nome e location. Il set delle facilities è
     * vuoto. L'aula non ha inizialmente nessuna prenotazione.
     *
     * @param nome     il nome dell'aula
     * @param location la location dell'aula
     *
     * @throws NullPointerException se una qualsiasi delle informazioni richieste è
     *                              nulla
     */
    public Aula(String nome, String location) {
        if (location == null || nome == null)
            throw new NullPointerException();   // controllo che non siano stati passati valori nulli
        this.location = location;               // inizializzazione attributo location
        this.nome = nome;                       // inizializzazione attributo nome
        this.prenotazioni = new TreeSet<Prenotazione>();    // inizializzazione attributo prenotazioni
        this.facilities = new HashSet<Facility>();  // inizializzazione attributo facilities
    }

    /**
     * Costruisce una certa aula con nome, location e insieme delle facilities.
     * L'aula non ha inizialmente nessuna prenotazione.
     *
     * @param nome       il nome dell'aula
     * @param location   la location dell'aula
     * @param facilities l'insieme delle facilities dell'aula
     * @throws NullPointerException se una qualsiasi delle informazioni richieste è
     *                              nulla
     */
    public Aula(String nome, String location, Set<Facility> facilities) {
        if (location == null || nome == null || facilities == null)
            throw new NullPointerException();   // controllo che non siano stati passati valori nulli
        this.location = location;   // inizializzazione attributo location
        this.nome = nome;   // inizializzazione attributo nome
        this.prenotazioni = new TreeSet<Prenotazione>();    // inizializzazione attributo prenotazione
        this.facilities = facilities;   // inizializzazione attributo facilities
    }

    /*
     * Ridefinire in accordo con equals
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.getNome());    // utilizzo il metodo hash di Object per definire questo metodo in accordo con equals
    }

    /* Due aule sono uguali se e solo se hanno lo stesso nome */
    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;    // se this è uguale a obj allora sarà sicuramente lo stesso oggetto
        if (!(object instanceof Aula))
            return false;   // se l'oggetto passato non è di tipo Aula la condizione non potrà essere verificata
        Aula aula = (Aula) object;  // eseguo il cast di object forzandolo in tipo Aula
        return getNome().equals(aula.getNome());
    }

    /* L'ordinamento naturale si basa sul nome dell'aula */
    @Override
    public int compareTo(Aula o) {
        return this.getNome().compareTo(o.getNome());
    }

    /**
     * @return the facilities
     */
    public Set<Facility> getFacilities() {
        return this.facilities;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * @return the location
     */
    public String getLocation() {
        return this.location;
    }

    /**
     * @return the prenotazioni
     */
    public SortedSet<Prenotazione> getPrenotazioni() {
        return this.prenotazioni;
    }

    /**
     * Aggiunge una faciltity a questa aula.
     *
     * @param f la facility da aggiungere
     * @return true se la facility non era già presente e quindi è stata aggiunta,
     *         false altrimenti
     * @throws NullPointerException se la facility passata è nulla
     */
    public boolean addFacility(Facility f) {
        if (f == null)
            throw new NullPointerException();   // controllo che non sia stato passato un valore nullo
        return this.facilities.add(f);  // aggiungo a facilities la facility passata
    }

    /**
     * Determina se l'aula è libera in un certo time slot.
     *
     *
     * @param ts il time slot da controllare
     *
     * @return true se l'aula risulta libera per tutto il periodo del time slot
     *         specificato
     * @throws NullPointerException se il time slot passato è nullo
     */
    public boolean isFree(TimeSlot ts) {
        // sfruttare l'ordinamento tra le prenotazioni per
        // rispondere in maniera efficiente
        if (ts == null)
            throw new NullPointerException(); // controllo che non sia stato passato un valore nullo

        if (this.getPrenotazioni().size() > 0) { // se non ci sono prenotazioni allora sicuramente isFree
            // essendo una lista ordinata, posso iniziare controllando
            // se il timeslot passato è precedente al primo o successivo
            // all'ultimo timeslot già presente nella SortedSet
            if (ts.getStop().before(this.getPrenotazioni().first().getTimeSlot().getStart())
                    || ts.getStart().after(this.getPrenotazioni().last().getTimeSlot().getStop())) {
                return true;
            }
            boolean isFree = false; // imposto di default la variabile isFree come false
            for (Prenotazione prenotazione : prenotazioni) // ciclo tutte le prenotazioni
                /*
                 * NOTA ooverlapsWith ritorna true se si sovrappongono. |= sovrascrive isFree
                 * solo se è vero
                 */
                isFree |= ts.overlapsWith(prenotazione.getTimeSlot());
            return !isFree; // ritorno ! IsFree perchè overlapsWith torna true solo se si sovrappongono e non il
                            // contrario
        }
        return true;
    }

    /**
     * Determina se questa aula soddisfa tutte le facilities richieste rappresentate
     * da un certo insieme dato.
     *
     * @param requestedFacilities l'insieme di facilities richieste da soddisfare
     * @return true se e solo se tutte le facilities di {@code requestedFacilities}
     *         sono soddisfatte da questa aula.
     * @throws NullPointerException se il set di facility richieste è nullo
     */
    public boolean satisfiesFacilities(Set<Facility> requestedFacilities) {
        if (requestedFacilities == null)
            throw new NullPointerException(); // controllo che non sia stato passato un valore nullo

        boolean satisfies = false;  // imposto satisfie a false come valore di default
        boolean returner = true; // imposto returner a true come valore di default
        Set<Facility> fcs = this.getFacilities(); // attribuisco alla variabile fcs tutte le facilities associate a questo oggetto
        for (Facility facility : requestedFacilities) { // ciclo le facilities passate al metodo
            satisfies = false;
            for (Facility fc : fcs) { // cicolo tutte le facilities di questo oggetto
                satisfies |= facility.equals(fc); // utilizzo |= per sovrascrivere satisfies solo se è true
            }
            returner &= satisfies; // returner è vero solo se ogni satisfies che viene paragonata dal ciclo è vera
        }
        return returner;
    }

    /**
     * Prenota l'aula controllando eventuali sovrapposizioni.
     *
     * @param ts
     * @param docente
     * @param motivo
     * @throws IllegalArgumentException se la prenotazione comporta una
     *                                  sovrapposizione con un'altra prenotazione
     *                                  nella stessa aula.
     * @throws NullPointerException     se una qualsiasi delle informazioni
     *                                  richieste è nulla.
     */
    public void addPrenotazione(TimeSlot ts, String docente, String motivo) {
        if (ts == null || docente == null || motivo == null)
            throw new NullPointerException(); // controllo che non siano stati passati valori nulli
        for (Prenotazione prenotazione : prenotazioni) { // ciclo le prenotazioni
            if (prenotazione.getTimeSlot().overlapsWith(ts)) // controllo che non ci siano prenotazioni nello stesso timeSlot
                throw new IllegalArgumentException();
        }
        this.getPrenotazioni().add(new Prenotazione(this, ts, docente, motivo)); // aggiungo la prenotazione
    }

    /**
     * Cancella una prenotazione di questa aula.
     *
     * @param p la prenotazione da cancellare
     * @return true se la prenotazione è stata cancellata, false se non era
     *         presente.
     * @throws NullPointerException se la prenotazione passata è null
     */
    public boolean removePrenotazione(Prenotazione p) {
        if (p == null)
            throw new NullPointerException(); // controllo che non siano stati passati valori nulli
        return this.getPrenotazioni().remove(p);
    }

    /**
     * Rimuove tutte le prenotazioni di questa aula che iniziano prima (o
     * esattamente in) di un punto nel tempo specificato.
     *
     * @param timePoint un certo punto nel tempo
     * @return true se almeno una prenotazione è stata cancellata, false altrimenti.
     * @throws NullPointerException se il punto nel tempo passato è nullo.
     */
    public boolean removePrenotazioniBefore(GregorianCalendar timePoint) {
        // sfruttare l'ordinamento tra le prenotazioni per
        // eseguire in maniera efficiente
        if (timePoint == null)
            throw new NullPointerException(); // controllo che non siano stati passati valori nulli

        Set<Prenotazione> toDelete = new TreeSet<Prenotazione>(); // inizializzo una variabile che dovrà contenere tutte le prenotazioni da cancellare
        for (Prenotazione prenotazione : this.getPrenotazioni()) { // ciclo le prenotazioni dell'oggetto this
            if (prenotazione.getTimeSlot().getStart().compareTo(timePoint) <= 0) { // se la prenotazione inizia prima del timeslot passato
                // aggiungo al Set per la cancellazione
                toDelete.add(prenotazione); // aggiungo alle prenotazioni da cancellare
            } else {
                break; // altrimenti salto il ciclo
            }
        }
        // inizializzo la variabile per il check della cancellazione
        boolean removed = false;
        if (toDelete.size() > 0) { // se toDelete contiene valori
            // rimuovo in blocco tutti gli elementi scelti
            this.getPrenotazioni().removeAll(toDelete); // provvedo all'eliminazione delle prenotazioni
            removed = true;
        }
        return removed; // ritorno un valore booleano per indicare se sono state cancellate delle prenotazioni o meno
    }
}
