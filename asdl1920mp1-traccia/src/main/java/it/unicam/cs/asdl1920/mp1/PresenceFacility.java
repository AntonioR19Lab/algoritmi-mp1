/**
 * 
 */
package it.unicam.cs.asdl1920.mp1;

/**
 * Una Presence Facility è una facility che può essere presente oppure no. Ad
 * esempio la presenza di un proiettore HDMI oppure la presenza dell'aria
 * condizionata.
 *
 * @author Name: ANTONIO
 *         Surname: RICCIO
 *         Email: antonio.riccio@studenti.unicam.it
 */
public class PresenceFacility extends Facility {

    /**
     * Costruisce una presence facility.
     * 
     * @param codice
     * @param descrizione
     * @throws NullPointerException
     *                                  se una qualsiasi delle informazioni
     *                                  richieste è nulla.
     */
    public PresenceFacility(String codice, String descrizione) {
        super(codice, descrizione); // utilizzo il costruttore della super classe Facility
    }

    /*
     * Una Presence Facility soddisfa una facility solo se la facility passata è
     * una Presence Facility ed ha lo stesso codice.
     * 
     */
    @Override
    public boolean satisfies(Facility o) {
    	if(o == null) throw new NullPointerException(); // verifco che non siano stati passati valori nulli
        if((o instanceof PresenceFacility && o.getCodice().equals(this.getCodice()))) return true; // se l'oggetto passato è di tipo PresenceFacility e equals torna true allora torno ture
        return false; // altrimenti torno false
    }

}
